using tech_test_payment_api.Exceptions;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Services;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.UnitTests.Services
{
    public class SalesmanServiceTests
    {
        private readonly ISalesmanService _salesmanService;
        private readonly Mock<ISaleRepository> _saleRepository;

        public SalesmanServiceTests()
        {
            _saleRepository = new Mock<ISaleRepository>();
            _salesmanService = new SalesmanService(_saleRepository.Object);
        }

        private static Sale SaleBankValid()
        {
            var saleBank = new Sale
            {
                Salesman = new Salesman
                {
                    SalesmanId = 1,
                    SalesmanName = "Salesman Bank",
                    SalesmanEmail = "salesmanbank@email.com",
                    SalesmanPhone = "62988888888",
                    SalesmanDocument = "46031181550"
                }
            };

            return saleBank;
        }

        [Fact]
        public void SalesmanValidate_WhenDocumentIsNull_ThrowException()
        {
            var salesmanDocumentNull = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesman@email.com",
                SalesmanPhone = "62999999999"
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Assert.Throws<BusinessException>(() => _salesmanService.SalesmanValidate(salesmanDocumentNull));

            //Assert
            Assert.Equal("O campo documento n�o deve ser vazio.", exception.Message);
        }

        [Fact]
        public void SalesmanValidate_WhenDocumentIsEmpty_ThrowException()
        {
            var salesmanDocumentIsEmpty = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesman@email.com",
                SalesmanPhone = "62999999999",
                SalesmanDocument = ""
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Assert.Throws<BusinessException>(() => _salesmanService.SalesmanValidate(salesmanDocumentIsEmpty));

            //Assert
            Assert.Equal("O campo documento n�o deve ser vazio.", exception.Message);
        }

        [Fact]
        public void SalesmanValidate_WhenDocumentIsInvalid_ThrowException()
        {
            var salesmanDocumentIsInvalid = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesman@email.com",
                SalesmanPhone = "62999999999",
                SalesmanDocument = "53979992887"
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Assert.Throws<BusinessException>(() => _salesmanService.SalesmanValidate(salesmanDocumentIsInvalid));

            //Assert
            Assert.Equal("O documento inserido � inv�lido.", exception.Message);
        }

        [Fact]
        public void SalesmanValidate_WhenDocumentIsOnDatabase_ThrowException()
        {
            var salesmanDocumentOnDatabase = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesman@email.com",
                SalesmanPhone = "62999999999",
                SalesmanDocument = "46031181550"
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Assert.Throws<BusinessException>(() => _salesmanService.SalesmanValidate(salesmanDocumentOnDatabase));

            //Assert
            Assert.Equal("O documento inserido j� est� vinculado � outro vendedor no banco de dados.", exception.Message);
        }

        [Fact]
        public void SalesmanValidate_WhenPhoneIsOnDatabase_ThrowException()
        {
            var salesmanPhoneOnDatabase = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesman@email.com",
                SalesmanPhone = "62988888888",
                SalesmanDocument = "50841167940"
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Assert.Throws<BusinessException>(() => _salesmanService.SalesmanValidate(salesmanPhoneOnDatabase));

            //Assert
            Assert.Equal("O telefone inserido j� est� vinculado � outro vendedor no banco de dados.", exception.Message);
        }

        [Fact]
        public void SalesmanValidate_WhenEmailIsOnDatabase_ThrowException()
        {
            var salesmanEmailOnDatabase = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesmanbank@email.com",
                SalesmanPhone = "62999999999",
                SalesmanDocument = "50841167940"
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Assert.Throws<BusinessException>(() => _salesmanService.SalesmanValidate(salesmanEmailOnDatabase));

            //Assert
            Assert.Equal("O email inserido j� est� vinculado � outro vendedor no banco de dados.", exception.Message);
        }

        [Fact]
        public void SalesmanValidate_WhenAllIsValid_Success()
        {
            var salesmanAllPropIsValid = new Salesman
            {
                SalesmanId = 2,
                SalesmanName = "Salesman",
                SalesmanEmail = "salesman@email.com",
                SalesmanPhone = "62999999999",
                SalesmanDocument = "50841167940"
            };

            var saleBankValid = SaleBankValid();

            //Arrange
            _saleRepository.Setup(x =>
                x.FindSaleByInfo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(saleBankValid);

            //Act
            var exception = Record.Exception(() => _salesmanService.SalesmanValidate(salesmanAllPropIsValid));

            //Assert
            Assert.Null(exception);
        }
    }
}

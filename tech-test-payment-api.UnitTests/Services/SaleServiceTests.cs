﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using tech_test_payment_api.Exceptions;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Services;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.UnitTests.Services
{
    public class SaleServiceTests
    {
        private readonly Mock<ISaleRepository> _saleRepository;
        private readonly Mock<ISalesmanService> _salesmanService;
        private readonly SaleService _saleService;

        public SaleServiceTests()
        {
            _saleRepository = new Mock<ISaleRepository>();
            _salesmanService = new Mock<ISalesmanService>();
            _saleService = new SaleService(_saleRepository.Object, _salesmanService.Object);
        }

        public static Sale[] SaleBankListValid()
        {
            var saleBankList = new Sale[]
            {
               new Sale { SaleId= 1 },
               new Sale { SaleId= 2 },
               new Sale { SaleId= 3 }
            };

            return saleBankList;
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task GetSale_WhenIdIsInvalid_ThrowException(int value)
        {
            //Arrange
            //Act
            async Task act() => await _saleService.GetSale(value);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Contains("O ID da venda é inválido.", exception.Message);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(5)]
        public async Task GetSale_WhenSearchResultIsNull_ThrowException(int value)
        {
            var saleNull = new Sale();
            saleNull = null;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleNull);

            //Act
            async Task act() => await _saleService.GetSale(value);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Equal($"A venda para o ID {value} não foi encontrada no banco de dados.", exception.Message);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetSale_WhenIdIsValid_Success(int value)
        {
            var SaleBank = new Sale() { SaleId = value };
            
            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(SaleBank);

            //Act
            async Task<Sale> act() => await _saleService.GetSale(value);
            var saleResult = act().Result;
            var exception = await Record.ExceptionAsync(act);

            //Assert
            Assert.Null(exception);
            Assert.Equal(value, saleResult.SaleId);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task SaleCreate_WhenIdAlreadyIsOnDatabase_ThrowException(int value)
        {
            var SaleBank = new Sale() { SaleId = value };
            
            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(SaleBank);

            //Act
            async Task act() => await _saleService.SaleCreate(SaleBank);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Equal("Operação inválida, ID da venda já consta no banco de dados.", exception.Message);
        }

        [Fact]
        public async Task SaleCreate_WhenAllIsValid_Success()
        {
            var newSale = new Sale() { Salesman = new Salesman() { SalesmanEmail = "Salesman@email.com" } };
            var saleNull = new Sale();
            saleNull = null;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleNull);
            _saleRepository.Setup(sr => sr.Save(It.IsAny<Sale>())).Returns(Task.FromResult(newSale));
         
            //Act
            async Task<Sale> act() => await _saleService.SaleCreate(newSale);
            var exception = await Record.ExceptionAsync(act);
            var saleResult = act().Result;

            //Assert
            Assert.NotNull(saleResult);
            Assert.Null(exception);
            _saleRepository.Verify(sr => sr.Save(It.IsAny<Sale>()), Times.Exactly(2));
            Assert.Equal("salesman@email.com", act().Result.Salesman.SalesmanEmail);
            Assert.Equal(EnumSaleStatus.AguardandoPagamento, saleResult.SaleStatus);
            Assert.NotEmpty(saleResult.Order);
        }

        [Fact]
        public async Task SaleUpdate_WhenIdIsInvalid_ThrowException()
        {
            var saleNull = new Sale();
            saleNull = null;

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleNull);

            //Act
            async Task act() => await _saleService.SaleUpdate( idToUpdate, EnumSaleStatus.AguardandoPagamento);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Equal($"A venda para o ID {idToUpdate} não foi encontrada no banco de dados.", exception.Message);
        }

        [Theory]
        [InlineData(EnumSaleStatus.AguardandoPagamento)]
        [InlineData(EnumSaleStatus.PagamentoAprovado)]
        [InlineData(EnumSaleStatus.EnviadoParaTransportadora)]
        [InlineData(EnumSaleStatus.Entregue)]
        [InlineData(EnumSaleStatus.Cancelada)]
        public async Task SaleUpdate_WhenIsEntregueToOther_ThrowException(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.Entregue };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);
            //Act
            async Task act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Contains($"O status da venda: {saleBank.SaleStatus}, não pode ser alterado.", exception.Message);
        }

        [Theory]
        [InlineData(EnumSaleStatus.AguardandoPagamento)]
        [InlineData(EnumSaleStatus.PagamentoAprovado)]
        [InlineData(EnumSaleStatus.EnviadoParaTransportadora)]
        [InlineData(EnumSaleStatus.Entregue)]
        [InlineData(EnumSaleStatus.Cancelada)]
        public async Task SaleUpdate_WhenIsCanceladaToOther_ThrowException(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.Cancelada };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);
            
            //Assert
            Assert.Contains($"O status da venda: {saleBank.SaleStatus}, não pode ser alterado.", exception.Message);
        }

        [Theory]
        [InlineData(EnumSaleStatus.AguardandoPagamento)]
        [InlineData(EnumSaleStatus.EnviadoParaTransportadora)]
        [InlineData(EnumSaleStatus.Entregue)]
        public async Task SaleUpdate_WhenIsAguardandoPagamentoToInvalid_ThrowException(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.AguardandoPagamento };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Contains($"O status da venda: {saleBank.SaleStatus} não pode ser alterado para: {saleStatus}.", exception.Message);
        }

        [Theory]
        [InlineData(EnumSaleStatus.PagamentoAprovado)]
        [InlineData(EnumSaleStatus.Cancelada)]
        public async Task SaleUpdate_WhenIsAguardandoPagamentoToValid_Success(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.AguardandoPagamento };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task<Sale> act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);

            var saleResult = act().Result;

            //Assert
            Assert.Equal(saleStatus, saleResult.SaleStatus);
        }

        [Theory]
        [InlineData(EnumSaleStatus.AguardandoPagamento)]
        [InlineData(EnumSaleStatus.PagamentoAprovado)]
        [InlineData(EnumSaleStatus.Entregue)]
        public async Task SaleUpdate_WhenIsPagamentoAprovadoToInvalid_ThrowException(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.PagamentoAprovado };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);

            //Assert
            Assert.Contains($"O status da venda: {saleBank.SaleStatus} não pode ser alterado para: {saleStatus}.", exception.Message);
        }

        [Theory]
        [InlineData(EnumSaleStatus.EnviadoParaTransportadora)]
        [InlineData(EnumSaleStatus.Cancelada)]
        public async Task SaleUpdate_WhenIsPagamentoAprovadoToValid_Success(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.PagamentoAprovado };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task<Sale> act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);
            var saleResult = act().Result;

            //Assert
            Assert.Equal(saleStatus, saleResult.SaleStatus);
        }

        [Theory]
        [InlineData(EnumSaleStatus.AguardandoPagamento)]
        [InlineData(EnumSaleStatus.PagamentoAprovado)]
        [InlineData(EnumSaleStatus.EnviadoParaTransportadora)]
        [InlineData(EnumSaleStatus.Cancelada)]
        public async Task SaleUpdate_WhenIsEnviadoParaTransportadora_ThrowException(EnumSaleStatus saleStatus)
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.EnviadoParaTransportadora };

            int idToUpdate = 1;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task<Sale> act() => await _saleService.SaleUpdate(idToUpdate, saleStatus);
            var exception = await Assert.ThrowsAsync<BusinessException>(act);
            
            //Assert
            Assert.Contains($"O status da venda: {saleBank.SaleStatus} não pode ser alterado para: {saleStatus}.", exception.Message);
        }

        [Fact]
        public async Task SaleUpdate_WhenIsEnviadoParaTransportadoraToValid_Success()
        {
            var saleBank = new Sale() { SaleId = 1, SaleStatus = EnumSaleStatus.EnviadoParaTransportadora };

            int idToUpdate = 1;

            EnumSaleStatus statusToUpdate = EnumSaleStatus.Entregue;

            //Arrange
            _saleRepository.Setup(sr => sr.Get(It.IsAny<int>())).ReturnsAsync(saleBank);

            //Act
            async Task<Sale> act() => await _saleService.SaleUpdate(idToUpdate, statusToUpdate);
            var saleResult = act().Result;

            //Assert
            Assert.Equal(statusToUpdate, saleResult.SaleStatus);
        }
    }   
}

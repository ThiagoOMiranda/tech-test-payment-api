﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Exceptions;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Dtos;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentApiController : ControllerBase
    {
        private readonly ISaleService _saleService;
        private readonly IMapper _mapper;

        public PaymentApiController(ISaleService saleService, IMapper mapper)
        {
            _saleService = saleService;
            _mapper = mapper;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Sale>> GetById(int id)
        {
            try
            {
                var saleBank = await _saleService.GetSale(id);
                return Ok(saleBank);
            }
            catch (BusinessException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<Sale>> CreateSale([FromBody]SaleDto saleDto)
        {
            try
            {
                var newSale = _mapper.Map<Sale>(saleDto);
                await _saleService.SaleCreate(newSale);
                return Ok(newSale);
            }
            catch (BusinessException e)
            {
                return BadRequest(e.Message);
            } 
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult<Sale>> SaleUpdate(int id, EnumSaleStatus saleStatus)
        {
            try
            {
                var saleToUpdate = await _saleService.SaleUpdate(id, saleStatus);
                return Ok(saleToUpdate);
            }
            catch (BusinessException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class PaymentApiContext : DbContext
    {
        public PaymentApiContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleItem> SalesItems { get; set; }
        public DbSet<Salesman> Salesmen { get; set; }
    }
}

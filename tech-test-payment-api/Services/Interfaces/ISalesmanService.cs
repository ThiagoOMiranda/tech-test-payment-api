﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services.Interfaces
{
    public interface ISalesmanService
    {
        public void SalesmanValidate(Salesman salesman);
    }
}

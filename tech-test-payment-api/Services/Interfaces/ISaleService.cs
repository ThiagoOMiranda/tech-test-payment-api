﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services.Interfaces
{
    public interface ISaleService
    {
        public Task<Sale> GetSale(int id);
        public Task<Sale> SaleCreate(Sale sale);
        public Task<Sale> SaleUpdate(int id, EnumSaleStatus saleStatus);
    }
}

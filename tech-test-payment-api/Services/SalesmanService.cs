﻿using DocumentValidator;
using tech_test_payment_api.Exceptions;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Services
{
    public class SalesmanService : ISalesmanService
    {
        private readonly ISaleRepository _saleRepository;

        public SalesmanService(ISaleRepository saleRepository)
        {
            _saleRepository = saleRepository;
        }

        private static void DocumentValidate(string document)
        {
            if(document == null || document == "")
                    throw new BusinessException("O campo documento não deve ser vazio.");

            var documentIsValid = CpfValidation.Validate(document);

            if (!documentIsValid)
                throw new BusinessException("O documento inserido é inválido.");
        }

        public void SalesmanValidate(Salesman salesman)
        {
            string name = salesman.SalesmanName;
            string phone = salesman.SalesmanPhone;
            string document = salesman.SalesmanDocument;
            string email = salesman.SalesmanEmail;

            DocumentValidate(document);

            var saleByDoc = _saleRepository.FindSaleByInfo(document, email, phone, "document");
            var saleDocIsInvalid = saleByDoc != null &&
                                   saleByDoc.Salesman.SalesmanDocument == document &&
                                   saleByDoc.Salesman.SalesmanName != name;
            if (saleDocIsInvalid)
                throw new BusinessException("O documento inserido já está vinculado à outro vendedor no banco de dados.");

            var saleByPhone = _saleRepository.FindSaleByInfo(document, email, phone, "phone");
            var salePhoneIsInvalid = saleByPhone != null &&
                                     saleByPhone.Salesman.SalesmanPhone == phone &&
                                     saleByPhone.Salesman.SalesmanDocument != document;
            if (salePhoneIsInvalid)
                throw new BusinessException("O telefone inserido já está vinculado à outro vendedor no banco de dados.");

            var saleByEmail = _saleRepository.FindSaleByInfo(document, email, phone, "email");
            var saleEmailIsInvalid = saleByEmail != null &&
                                     saleByEmail.Salesman.SalesmanEmail == email &&
                                     saleByEmail.Salesman.SalesmanDocument != document;
            if (saleEmailIsInvalid)
                throw new BusinessException("O email inserido já está vinculado à outro vendedor no banco de dados.");
        }
    }
}

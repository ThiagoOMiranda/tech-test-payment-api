﻿using System.Diagnostics;
using tech_test_payment_api.Exceptions;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Services
{
    public class SaleService : ISaleService
    {
        private readonly ISaleRepository _saleRepository;
        private readonly ISalesmanService _salesmanService;

        public SaleService(ISaleRepository saleRepository, ISalesmanService salesmanService)
        {
            _saleRepository = saleRepository;
            _salesmanService = salesmanService;
        }
        
        public async Task<Sale> GetSale(int id)
        {
            if (id <= 0)
                throw new BusinessException("O ID da venda é inválido.");

            var saleBank = await _saleRepository.Get(id);
            if (saleBank == null)
                throw new BusinessException($"A venda para o ID {id} não foi encontrada no banco de dados.");
        
            return saleBank;
        }

        public async Task<Sale> SaleCreate(Sale sale)
        {
            var saleCheck = await _saleRepository.Get(sale.SaleId);

            if (saleCheck != null)
                throw new BusinessException("Operação inválida, ID da venda já consta no banco de dados.");

            sale.Order = GetOrder();
            sale.SaleDate= DateTime.Now;
            sale.SaleStatus = EnumSaleStatus.AguardandoPagamento;
            sale.Salesman.SalesmanEmail = sale.Salesman.SalesmanEmail.ToLower();
            
            SaleValidate(sale);

            return await _saleRepository.Save(sale);
        }

        public async Task<Sale> SaleUpdate(int id, EnumSaleStatus saleStatus)
        {
            var saleBank = await GetSale(id);
            ValidateStatus(saleBank, saleStatus);
            saleBank.SaleStatus = saleStatus;
            await _saleRepository.Save(saleBank);

            return saleBank;
        }

        private static string GetOrder()
        {
            Int32 timeStamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            return timeStamp.ToString();
        }

        private static void ValidateStatus(Sale saleBank, EnumSaleStatus saleStatus)
        {
            if (saleBank.IsEntregue(saleBank.SaleStatus) || saleBank.IsCancelada(saleBank.SaleStatus))
                throw new BusinessException($"O status da venda: {saleBank.SaleStatus}, não pode ser alterado.");

            if (saleBank.IsAguardandoPagamento(saleBank.SaleStatus)
                && saleStatus != EnumSaleStatus.Cancelada
                && saleStatus != EnumSaleStatus.PagamentoAprovado)
                throw new BusinessException($"O status da venda: {saleBank.SaleStatus} não pode ser alterado para: {saleStatus}.");

            if (saleBank.IsPagamentoAprovado(saleBank.SaleStatus)
                && saleStatus != EnumSaleStatus.Cancelada
                && saleStatus != EnumSaleStatus.EnviadoParaTransportadora)
                throw new BusinessException($"O status da venda: {saleBank.SaleStatus} não pode ser alterado para: {saleStatus}.");

            if (saleBank.IsEnviadoParaTransportadora(saleBank.SaleStatus)
                && saleStatus != EnumSaleStatus.Entregue)
                throw new BusinessException($"O status da venda: {saleBank.SaleStatus} não pode ser alterado para: {saleStatus}.");
        }

        private void SaleValidate(Sale sale)
        {
            Salesman NewSalesman = sale.Salesman;
            _salesmanService.SalesmanValidate(NewSalesman);
        }
    }
}

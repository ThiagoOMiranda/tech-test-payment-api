﻿using FluentValidation;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Validators
{
    internal class SalesmanValidator : AbstractValidator<Salesman>
    {
        public SalesmanValidator()
        {
            RuleFor(salesman => salesman.SalesmanName)
                .NotNull()
                .NotEmpty()
                .Length(3, 250);

            RuleFor(salesman => salesman.SalesmanEmail)
                .EmailAddress()
                .NotNull()
                .NotEmpty()
                .MaximumLength(150);

            RuleFor(salesman => salesman.SalesmanPhone)
                .NotNull()
                .NotEmpty()
                .Matches("^(?:(?:\\+|00)?(55)\\s?)?(?:\\(?([1-9][0-9])\\)?\\s?)?(?:((?:9\\d|[2-9])\\d{3})\\-?(\\d{4}))$")
                .Length(11);

            RuleFor(salesman => salesman.SalesmanDocument)
                .NotNull()
                .NotEmpty()
                .Matches("^[0-9]*$")
                .Length(11);
        }
    }
}

﻿using FluentValidation;
using tech_test_payment_api.Models.Dtos;

namespace tech_test_payment_api.Validators
{
    public class SaleDtoValidator : AbstractValidator<SaleDto>
    {
        public SaleDtoValidator()
        {
            RuleFor(sale => sale.SaleItems)
                .NotNull()
                .NotEmpty()
                .ForEach(saleItem =>
                {
                    saleItem.SetValidator(new SaleItemDtoValidator());
                });

            RuleFor(sale => sale.Salesman)
                .NotNull()
                .NotEmpty()
                .SetValidator(new SalesmanDtoValidator());
        }
    }
}

﻿using FluentValidation;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Validators
{
    public class SaleItemValidator : AbstractValidator<SaleItem>
    {
        public SaleItemValidator()
        {
            RuleFor(saleItem => saleItem.SaleItemDescription)
                .NotNull()
                .NotEmpty()
                .Length(5, 250);
        }
    }
}

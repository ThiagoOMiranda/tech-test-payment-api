﻿using FluentValidation;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Validators
{
    public class SaleValidator : AbstractValidator<Sale>
    {
        public SaleValidator()
        {
            RuleFor(sale => sale.SaleItems)
                .NotNull()
                .NotEmpty()
                .ForEach(SaleItem =>
                {
                    SaleItem.SetValidator(new SaleItemValidator());
                });

            RuleFor(sale => sale.Salesman)
                .NotNull()
                .NotEmpty()
                .SetValidator(new SalesmanValidator());
        }
    }
}

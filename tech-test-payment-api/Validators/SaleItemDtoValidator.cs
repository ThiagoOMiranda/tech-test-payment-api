﻿using FluentValidation;
using tech_test_payment_api.Models.Dtos;

namespace tech_test_payment_api.Validators
{
    public class SaleItemDtoValidator : AbstractValidator<SaleItemDto>
    {
        public SaleItemDtoValidator()
        {
            RuleFor(x => x.SaleDescription)
                .NotNull()
                .NotEmpty()
                .Length(5, 250);
        }
    }
}

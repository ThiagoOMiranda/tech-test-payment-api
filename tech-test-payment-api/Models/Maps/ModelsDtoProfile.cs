﻿using AutoMapper;
using tech_test_payment_api.Models.Dtos;

namespace tech_test_payment_api.Models.Maps
{
    public class ModelsDtoProfile : Profile
    {
        public ModelsDtoProfile()
        {
            CreateMap<Sale, SaleDto>().ReverseMap();
            CreateMap<SaleItem, SaleItemDto>().ReverseMap();
            CreateMap<Salesman, SalesmanDto>().ReverseMap();
        }
    }
}

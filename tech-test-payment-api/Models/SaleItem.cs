﻿using FluentValidation;

namespace tech_test_payment_api.Models
{
    public class SaleItem
    {
        public int SaleItemId { get; set; }
        public string SaleItemDescription { get; set; } = string.Empty;
    }
}

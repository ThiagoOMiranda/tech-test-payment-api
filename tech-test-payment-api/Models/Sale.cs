﻿using FluentValidation;

namespace tech_test_payment_api.Models
{
    public class Sale
    {
        public int SaleId { get; set; }
        public string Order { get; set; } = string.Empty;
        public DateTime SaleDate { get; set; }
        public virtual ICollection<SaleItem> SaleItems { get; set; } = new List<SaleItem>();
        public EnumSaleStatus SaleStatus { get; set; }
        public virtual Salesman Salesman { get; set; } = new Salesman();

        public bool IsAguardandoPagamento(EnumSaleStatus saleStaus)
        {
            return saleStaus == EnumSaleStatus.AguardandoPagamento;
        }

        public bool IsPagamentoAprovado(EnumSaleStatus saleStatus)
        {
            return saleStatus == EnumSaleStatus.PagamentoAprovado;
        }

        public bool IsEnviadoParaTransportadora(EnumSaleStatus saleStatus)
        {
            return saleStatus == EnumSaleStatus.EnviadoParaTransportadora;
        }

        public bool IsEntregue(EnumSaleStatus saleStatus)
        {
            return saleStatus == EnumSaleStatus.Entregue;
        }

        public bool IsCancelada(EnumSaleStatus saleStatus)
        {
            return saleStatus == EnumSaleStatus.Cancelada;
        }
    }
}

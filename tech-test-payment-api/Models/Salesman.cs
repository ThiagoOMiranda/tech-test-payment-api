﻿using FluentValidation;

namespace tech_test_payment_api.Models
{
    public class Salesman
    {
        public int SalesmanId { get; set; }
        public string SalesmanName { get; set; } = string.Empty;
        public string SalesmanDocument { get; set; } = string.Empty;
        public string SalesmanEmail { get; set; } = string.Empty;
        public string SalesmanPhone { get; set; } = string.Empty;
    }
}

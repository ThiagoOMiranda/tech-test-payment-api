﻿using FluentValidation;

namespace tech_test_payment_api.Models.Dtos
{
    public class SaleDto
    {
        public ICollection<SaleItemDto> SaleItems { get; set; } = new List<SaleItemDto>();
        public SalesmanDto Salesman { get; set; } = new SalesmanDto();
    }
}

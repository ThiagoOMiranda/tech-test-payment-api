﻿using FluentValidation;

namespace tech_test_payment_api.Models.Dtos
{
    public class SalesmanDto
    {
        public string SalesmanName { get; set; } = string.Empty;
        public string SalesmanDocument { get; set; } = string.Empty;
        public string SalesmanEmail { get; set; } = string.Empty;
        public string SalesmanPhone { get;set; } = string.Empty;
    }
}

﻿using FluentValidation;

namespace tech_test_payment_api.Models.Dtos
{
    public class SaleItemDto
    {
        public string? SaleDescription { get; set; }
    }
}

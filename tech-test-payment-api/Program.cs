using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Repositories;
using tech_test_payment_api.Services.Interfaces;
using tech_test_payment_api.Services;
using System.Text.Json.Serialization;
using FluentValidation;
using tech_test_payment_api.Models.Dtos;
using FluentValidation.AspNetCore;
using tech_test_payment_api.Validators;
using tech_test_payment_api.Models.Maps;

namespace tech_test_payment_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers()
                .AddJsonOptions(opt => opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
            
            builder.Services.AddScoped<IValidator<SaleDto>, SaleDtoValidator>();
            builder.Services.AddScoped<IValidator<SaleItemDto>, SaleItemDtoValidator>();
            builder.Services.AddScoped<IValidator<SalesmanDto>, SalesmanDtoValidator>();

            builder.Services.AddDbContext<PaymentApiContext>(opt => opt.UseInMemoryDatabase("salesDb"));
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddFluentValidationAutoValidation();
            builder.Services.AddFluentValidationClientsideAdapters();

            builder.Services.AddScoped<ISaleRepository, SaleRepository>();
            builder.Services.AddScoped<ISaleService, SaleService>();
            builder.Services.AddScoped<ISalesmanService, SalesmanService>();

            builder.Services.AddAutoMapper(typeof(ModelsDtoProfile));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();
            
            app.MapControllers();

            app.Run();
        }
    }
}
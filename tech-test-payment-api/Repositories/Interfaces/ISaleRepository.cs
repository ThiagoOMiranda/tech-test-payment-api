﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repositories.Interfaces
{
    public interface ISaleRepository
    {
        public Task<Sale?> Get(int id);
        public Task<Sale> Save(Sale sale);
        public Sale? FindSaleByInfo(string document, string email, string phone, string option);
    }
}

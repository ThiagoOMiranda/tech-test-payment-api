﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;

namespace tech_test_payment_api.Repositories
{
    public class SaleRepository : ISaleRepository
    {
        private readonly PaymentApiContext _context;

        public SaleRepository(PaymentApiContext context)
        {
            _context = context;
        }
        
        public async Task<Sale?> Get(int id)
        {
            var saleBank = await _context.Sales
                .Include(x => x.SaleItems)
                .Include(x => x.Salesman)
                .FirstOrDefaultAsync(x => x.SaleId == id);

            return saleBank;
        }

        public async Task<Sale> Save(Sale sale)
        {
            var saleBank = await Get(sale.SaleId);

            if(saleBank != null)
            {
                _context.Sales.Update(saleBank);
                await _context.SaveChangesAsync();

                return saleBank;
            }
            await _context.Sales.AddAsync(sale);
            await _context.SaveChangesAsync();

            return sale;
        }

        public Sale? FindSaleByInfo(string document, string email, string phone, string option)
        {
            var saleBank = new Sale();

            saleBank = option switch
            {
                "email" => _context.Sales.FirstOrDefault(x => x.Salesman.SalesmanEmail == email),
                "phone" => _context.Sales.FirstOrDefault(x => x.Salesman.SalesmanPhone == phone),
                "document" => _context.Sales.FirstOrDefault(x => x.Salesman.SalesmanDocument == document),
                _ => throw new ArgumentException("Parâmetro de busca inválido"),
            };

            return saleBank;
        }
    }
}
